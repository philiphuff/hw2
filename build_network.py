#!/usr/bin/env python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.cli import CLI
from mininet.node import Switch, OVSKernelSwitch
from time import sleep, time
from argparse import ArgumentParser
import os
import termcolor as T

setLogLevel('info')

parser = ArgumentParser("Configure simple network in Mininet.")
parser.add_argument('--rogue', action="store_true", default=False)
parser.add_argument('--sleep', default=3, type=int)
args = parser.parse_args()


def log(s, col="green"):
    print T.colored(s, col)


class Router(Switch):
    """Defines a new router that is inside a network namespace so that the
    individual routing entries don't collide.

    """
    ID = 0
    def __init__(self, name, **kwargs):
        kwargs['inNamespace'] = True
        Switch.__init__(self, name, **kwargs)
        Router.ID += 1
        self.switch_id = Router.ID

    @staticmethod
    def setup():
        return

    def start(self, controllers):
        pass

    def stop(self):
        self.deleteIntfs()

    def log(self, s, col="magenta"):
        print T.colored(s, col)


class SimpleTopo(Topo):
    def __init__(self):
        # Add default members to class.
        super(SimpleTopo, self ).__init__()
        num_hosts_per_as = 3
        num_ases = 3
        num_hosts = num_hosts_per_as * num_ases
        # The topology has one router per AS
	routers = []
        for i in xrange(num_ases):
            router = self.addSwitch('R%d' % (i+1))
	    routers.append(router)
        hosts = []
        for i in xrange(num_ases):
            router = 'R%d' % (i+1)
            for j in xrange(num_hosts_per_as):
                hostname = 'h%d-%d' % (i+1, j+1)
                host = self.addNode(hostname)
                hosts.append(host)
                self.addLink(router, host)
				
        self.addLink('R1', 'R2')
        self.addLink('R1', 'R3')

        return
		



def getIP(hostname):
    AS, idx = hostname.replace('h', '').split('-')
    AS = int(AS)
    ip = '10.%s.%s.1/24' % (AS, idx)
    return ip


def getGateway(hostname):
    AS, idx = hostname.replace('h', '').split('-')
    AS = int(AS)
    gw = '10.%s.%s.254' % (AS, idx)
    return gw


def main():
    #Cleanup any previous process
    os.system("rm -f /tmp/R*.log /tmp/R*.pid logs/*")
    os.system("mn -c >/dev/null 2>&1")
    os.system("killall -9 zebra > /dev/null 2>&1")
    os.system('pgrep -f webserver.py | xargs kill -9')

    #Build the network according to function SimpleTopo()
    net = Mininet(topo=SimpleTopo(), switch=Router)
    net.start()

    # By default, Linux disables IP forwarding. This is needed for the routers
    for router in net.switches:
        router.cmd("sysctl -w net.ipv4.ip_forward=1")
        router.waitOutput()

    log("Waiting %d seconds for sysctl changes to take effect..."
        % args.sleep)
    sleep(args.sleep)

    #Configuring the routers for BGP
    for router in net.switches:
        router.cmd("/usr/lib/quagga/zebra -f conf/zebra-%s.conf -d -i /tmp/zebra-%s.pid > logs/%s-zebra-stdout 2>&1" % (router.name, router.name, router.name))
        router.waitOutput()
        log("Starting zebra on %s" % router.name)

    #Creating the default gateways in each host
    for host in net.hosts:
        host.cmd("ifconfig %s-eth0 %s" % (host.name, getIP(host.name)))
        host.cmd("route add default gw %s" % (getGateway(host.name)))

    # Starting the web server
    log("Starting web servers", 'yellow')
    host = net.getNodeByName('h1-1')
    host.cmd("python -m SimpleHTTPServer 80 >& /tmp/http.log &")

    CLI(net)
    net.stop()
    os.system("killall -9 zebra")
    os.system('pgrep -f webserver.py | xargs kill -9')


if __name__ == "__main__":
    main()
